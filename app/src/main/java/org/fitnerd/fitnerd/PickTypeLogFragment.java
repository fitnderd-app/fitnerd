package org.fitnerd.fitnerd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.fitnerd.fitnerd.typelist.TypeExercise;

import java.util.Objects;

public class PickTypeLogFragment extends ListFragment {

    public PickTypeLogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<TypeExercise.Exercise> Adapter1 = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1, TypeExercise.ITEMS); {

        }

        setListAdapter(Adapter1);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onListItemClick(ListView listView, View view, int position,
                                long id) {
        // getResources().getString(R.string.app_name);
        Intent intent = new Intent(getContext(), PickExerciseLogActivity.class);
        intent.putExtra("TYPE", TypeExercise.ITEMS.get(position).content);
        if (getArguments() != null) {
            intent.putExtra("pickedDate", getArguments().getString("pickedDate"));
        }
        startActivity(intent);
    }

}
