package org.fitnerd.fitnerd;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class LogActivity extends AppCompatActivity implements OnClickListener {

    private EditText date;

    private DatePickerDialog fromDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        createAndSetToolbar();

        Log.d(getIntent().getStringExtra("pickedDate"), "selected date in LogActivity:");


        EditText pickedDate = findViewById(R.id.etxt_date);
        pickedDate.setText(getIntent().getStringExtra("pickedDate"));

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);

        findViewsById();

        setDateTimeField();

        // video stuff
        String frameVideo = "<html><body><iframe  width=\"100%\"  src=\"https://www.youtube.com/embed/TIfAkOBMf5A\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
        WebView displayYoutubeVideo = findViewById(R.id.mWebView);
        displayYoutubeVideo.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = displayYoutubeVideo.getSettings();
        webSettings.setJavaScriptEnabled(true);
        displayYoutubeVideo.loadData(frameVideo, "text/html", "utf-8");
        // video stuff

        Button button = findViewById(R.id.addButton);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getApplicationContext(), "Saved!", Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }

    private void createAndSetToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Objects.requireNonNull(getSupportActionBar()).getTitle() + " " + getIntent().getStringExtra("title"));
    }

    private void findViewsById() {
        date = findViewById(R.id.etxt_date);
        Log.d(getIntent().getStringExtra("pickedDate"), "selected date in LogActivity:");
        date.setInputType(InputType.TYPE_NULL);
        date.requestFocus();

    }

    private void setDateTimeField() {
        date.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        if(view == date) {
            fromDatePickerDialog.show();
        }
    }
}