package org.fitnerd.fitnerd;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.Objects;

public class PickExerciseLogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_exercise_log);

        Toolbar toolbar = findViewById(R.id.toolbar);

        Bundle bundle = new Bundle();
        bundle.putString("pickedDate", getIntent().getStringExtra("pickedDate"));

        PickExerciseLogFragment myObj = new PickExerciseLogFragment();
        Log.d(getIntent().getStringExtra("pickedDate"), "selected date in ExcerciseLogActivity:");
        setSupportActionBar(toolbar);
        String typeExercise = getIntent().getStringExtra("TYPE");
        getSupportActionBar().setTitle(Objects.requireNonNull(getSupportActionBar()).getTitle() + " " + typeExercise);

        bundle.putString("typeExercise", typeExercise);

        Log.d("AAAAA", "AAAAAAA");
        myObj.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, myObj).commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
