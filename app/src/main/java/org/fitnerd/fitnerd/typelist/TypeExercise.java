package org.fitnerd.fitnerd.typelist;

import java.util.ArrayList;
import java.util.List;

public class TypeExercise {

    public static List<Exercise> ITEMS = new ArrayList<>();

    static {
        addItem(new Exercise("1", "Abs", new Training[]{
                new Training("Burpee", "Reps/Time"),
                new Training("Crunch", "Reps/Time"),
                new Training("Crunch Machine", "Reps/Time"),
                new Training("Decline Crunch", "Reps/Time"),
                new Training("Suspended Crunch Push Up", "Reps/Time"),
                new Training("Hanging Knee Raise", "Reps/Time"),
                new Training("Hanging Leg Raise", "Reps/Time"),
                new Training("Inverted Crunch", "Reps/Time"),
                new Training("Oblique Crunch", "Reps/Time"),
                new Training("Plank", "Time"),
                new Training("Russian Twist", "Reps/Time"),
                new Training("Side Plank", "Time"),
                new Training("Sit Up", "Reps/Time"),
                new Training("Suspended Knee Raise", "Reps/Time"),
                new Training("Suspended Leg Raise", "Reps/Time"),
                new Training("Suspended Plank", "Time"),
                new Training("Weighted Sit Up", "Weight/Reps/Time")}));
        addItem(new Exercise("2", "Back", new Training[]{
                new Training("Barbell Row", "Weight/Reps"),
                new Training("Chin Up", "Reps"),
                new Training("Deadlift", "Weight/Reps"),
                new Training("Dumbbell Row", "Weight/Reps"),
                new Training("Lat Pulldown", "Weight/Reps"),
                new Training("Lat Pulldown Single Handle", "Weight/Reps"),
                new Training("Muscle Up", "Reps"),
                new Training("Pull Up", "Reps"),
                new Training("Straight-Arm Cable Pushdown", "Weight/Reps")}));
        addItem(new Exercise("3", "Biceps", new Training[]{
                new Training("Barbell Curl", "Weight/Reps"),
                new Training("Cable Curl", "Weight/Reps"),
                new Training("Dumbbell Concentration Curl", "Weight/Reps"),
                new Training("Dumbbell Curl", "Weight/Reps"),
                new Training("Dumbbell Hammer Curl", "Weight/Reps"),
                new Training("Dumbbell Preacher Curl", "Weight/Reps"),
                new Training("Rope Curl", "Weight/Reps"),
                new Training("Scott Bench", "Weight/Reps"),
                new Training("Seated Incline Dumbbell Curl", "Weight/Reps")}));
        addItem(new Exercise("4", "Chest", new Training[]{
                new Training("Cable Crossover", "Weight/Reps"),
                new Training("Decline Barbell Bench Press", "Weight/Reps"),
                new Training("Flat Barbell Bench Press", "Weight/Reps"),
                new Training("Flat Dumbbell Fly", "Weight/Reps"),
                new Training("Incline Barbell Bench Press", "Weight/Reps"),
                new Training("Incline Dumbbell Bench Press", "Weight/Reps"),
                new Training("Incline Dumbbell Fly", "Weight/Reps"),
                new Training("Incline Dumbbell Push", "Weight/Reps"),
                new Training("Roller Push Up", "Weight/Reps"),
                new Training("Suspended Roller Push Up", "Weight/Reps")}));
        addItem(new Exercise("5", "Legs", new Training[]{
                new Training("Barbell Calf Raise", "Weight/Reps"),
                new Training("Barbell Front Squat", "Weight/Reps"),
                new Training("Barbell Glute Bridge", "Weight/Reps"),
                new Training("Donkey Calf Raise", "Weight/Reps"),
                new Training("Glute-Ham Raise", "Weight/Reps"),
                new Training("Leg Extension Machine", "Weight/Reps"),
                new Training("Leg Press", "Weight/Reps"),
                new Training("Lying Leg Curl Machine", "Weight/Reps"),
                new Training("Romanian Deadlift", "Weight/Reps"),
                new Training("Seated Calf Raise Machine", "Weight/Reps"),
                new Training("Stiff-Legged Deadlift", "Weight/Reps"),
                new Training("Sumo Deadlift", "Weight/Reps")}));
        addItem(new Exercise("6", "Shoulders", new Training[]{
                new Training("Arnold Dumbbell Press", "Weight/Reps"),
                new Training("Cable Face Pull", "Weight/Reps"),
                new Training("Front Dumbbell Raise", "Weight/Reps"),
                new Training("Lateral Dumbbell Raise", "Weight/Reps"),
                new Training("One-Arm Standing Dumbbell Press", "Weight/Reps"),
                new Training("Overhead Press", "Weight/Reps"),
                new Training("Push Press", "Weight/Reps"),
                new Training("Rear Delt Dumbbell Raise", "Weight/Reps"),
                new Training("Scapular Retraction", "Weight/Reps"),
                new Training("Seated Dumbbell Lateral Raise", "Weight/Reps"),
                new Training("Seated Dumbbell Press", "Weight/Reps")}));
        addItem(new Exercise("7", "Triceps", new Training[]{
                new Training("Cable Overhead Triceps Extension", "Weight/Reps"),
                new Training("Closer Grip Barbell Bench Press", "Weight/Reps"),
                new Training("Dumbbell Overhead Triceps Extension", "Weight/Reps"),
                new Training("French Press", "Weight/Reps"),
                new Training("Lying Triceps Extension", "Weight/Reps"),
                new Training("Overhead Dumbbell French Press", "Weight/Reps"),
                new Training("Parallel Bar Triceps Dip", "Weight/Reps"),
                new Training("Ring Dip", "Weight/Reps"),
                new Training("Rope Push Down", "Weight/Reps")}));
        addItem(new Exercise("8", "Cardio", new Training[]{
                new Training("Cycling", "Distance/Time"),
                new Training("Elliptical Trainer", "Distance/Time"),
                new Training("Rowing Machine", "Distance/Time"),
                new Training("Running (Outdoor)", "Distance/Time"),
                new Training("Running (Treadmill)", "Distance/Time"),
                new Training("Stationary Bike", "Distance/Time"),
                new Training("Swimming", "Distance/Time"),
                new Training("Walking", "Distance/Time")}));
        addItem(new Exercise("9", "HIIT", new Training[]{
                new Training("Cycling 1:1", "Distance/Time"),
                new Training("Cycling 2:1", "Distance/Time"),
                new Training("Cycling 4:1", "Distance/Time"),
                new Training("Runnning (Outdoor) 1:1", "Distance/Time"),
                new Training("Runnning (Outdoor) 2:1", "Distance/Time"),
                new Training("Runnning (Outdoor) 4:1", "Distance/Time"),
                new Training("Runnning (Treadmill) 1:1", "Distance/Time"),
                new Training("Runnning (Treadmill) 2:1", "Distance/Time"),
                new Training("Runnning (Treadmill) 4:1", "Distance/Time"),
                new Training("Stationary Bike 1:1", "Distance/Time"),
                new Training("Stationary Bike 2:1", "Distance/Time"),
                new Training("Stationary Bike 4:1", "Distance/Time"),}));
    }

    public static List<String> getTraining(String name){
        List<String> TRAININGS = new ArrayList<>();
        for (Exercise item : ITEMS) {
            if (item.content.equalsIgnoreCase(name)) {
                for (Training training : item.exercisesType) {
                    TRAININGS.add(training.name);
                }
            }
        }
        if (TRAININGS.isEmpty()) {
            for (Training training : ITEMS.get(0).exercisesType) {
                TRAININGS.add(training.name);
            }
        }
        return TRAININGS;
    }

    private static void addItem(Exercise item) {
        ITEMS.add(item);
    }

    public static class Exercise {
        public String id;
        public String content;
        public Training[] exercisesType;

        Exercise(String id, String content, Training[] exercisesType) {
            this.id = id;
            this.content = content;
            this.exercisesType = exercisesType;
        }


        @Override
        public String toString() {

            return (content);
        }
    }

    static {

    }

    public static class Training {

        public String name;
        public String type;

        Training(String name, String type) {
            this.name = name;
            this.type = type;
        }
    }
}

