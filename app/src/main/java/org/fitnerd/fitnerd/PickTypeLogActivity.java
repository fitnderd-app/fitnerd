package org.fitnerd.fitnerd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.fitnerd.fitnerd.typelist.TypeExercise;

public class PickTypeLogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_type_log);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Log.d(getIntent().getStringExtra("pickedDate"), "selected date in PickTypeLogActivity:");

        Bundle bundle = new Bundle();
        bundle.putString("pickedDate", getIntent().getStringExtra("pickedDate"));

        PickTypeLogFragment myObj = new PickTypeLogFragment();
        myObj.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, myObj).commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

}
