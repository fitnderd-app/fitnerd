package org.fitnerd.fitnerd;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import org.fitnerd.fitnerd.typelist.TypeExercise;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        createAndSetToolbar();
        createAndSetCalendar();
        createAndSetButton();
    }

    private void createAndSetToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void createAndSetButton() {
        MaterialCalendarView mcv = findViewById(R.id.calendarView);
        mcv.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(2016, 1, 1))
                .setMaximumDate(CalendarDay.from(2099, 12, 31))
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();
        Date date = Calendar.getInstance().getTime();
        mcv.setCurrentDate(date);
        mcv.setDateSelected(date, true);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), PickTypeLogActivity.class);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
            intent.putExtra("pickedDate", dateFormatter.format(date));
            Log.d(dateFormatter.format(date), "selected date in HomeActivity:");
            startActivity(intent);
        });
    }

    private void createAndSetCalendar() {
        MaterialCalendarView mcv = findViewById(R.id.calendarView);
        mcv.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(2016, 1, 1))
                .setMaximumDate(CalendarDay.from(2099, 12, 31))
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();

        Date date = Calendar.getInstance().getTime();
        mcv.setCurrentDate(date);
        mcv.setDateSelected(date, true);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), PickTypeLogActivity.class);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
            intent.putExtra("pickedDate", dateFormatter.format(date));
            Log.d(dateFormatter.format(date), "selected date in HomeActivity:");
            startActivity(intent);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeActivity/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
