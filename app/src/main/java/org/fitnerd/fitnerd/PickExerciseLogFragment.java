package org.fitnerd.fitnerd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;
import java.util.Objects;

import static org.fitnerd.fitnerd.typelist.TypeExercise.getTraining;


public class PickExerciseLogFragment extends ListFragment {

    private List<String> trainings;

    public PickExerciseLogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            String typeExercise = getArguments().getString("typeExercise");
            Log.d(typeExercise, "Exercise: ");
            trainings = getTraining(typeExercise);
            ArrayAdapter<String> Adapter1 = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                    android.R.layout.simple_list_item_activated_1,
                    android.R.id.text1, trainings); {

            }

            setListAdapter(Adapter1);
        }
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position,
                                long id) {
        Intent intent = new Intent(getContext(), LogActivity.class);
        intent.putExtra("pickedDate", getArguments().getString("pickedDate"));
        intent.putExtra("title", trainings.get(position));
        startActivity(intent);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListShown(true);
    }
}


